import { Request, Response } from 'express';
import { Handler } from 'express';
import { register, all } from '../services/avatar';



// export class AvatarController {
//     constructor(private readonly avatarServices: avatarService) {}
    

  export const newAvatar: Handler = async(req: Request, res: Response) => {
         
      try {
        const avatar = await register(req.body);

        const result = avatar ? avatar : 'Fail Service';
            
       return res.json(result);

    } catch (err){
        console.log(`Fail Service ${err}`);
    }      


    }

    export const allAvatar: Handler = async(req: Request, res: Response) => {
        try {
          const avatar = await all()
          return res.json(avatar)
        } catch (err){
            console.log(`Fail Service ${err}`);
        }
    }


// }

