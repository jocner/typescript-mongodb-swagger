import express from 'express';
import cors from 'cors';
import morgan from 'morgan';
import conectarDB from './config/db';
import avatarRouter from './routes/avatar.router';
import swaggerUI from 'swagger-ui-express';
import swaggerJsDocs from 'swagger-jsdoc';
import { option } from './swaggerOptions';
import * as dotenv from 'dotenv';

dotenv.config();



const app = express();

conectarDB();

const PORT = process.env.PORT || 8000;

app.use(cors());
app.use(express.json());
app.use(morgan("dev"));

const specs = swaggerJsDocs(option);

app.use(avatarRouter);
app.use('/docs', swaggerUI.serve, swaggerUI.setup(specs));


app.listen(PORT, () => console.log(`The server is running on port ${PORT}`));