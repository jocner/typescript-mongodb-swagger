import { connect } from 'mongoose';
//const mongoose = require('mongoose');
// require('dotenv').config({ path: '../variables.env'});
import * as dotenv from 'dotenv';

dotenv.config();

const DB_MONGO = 'mongodb+srv://jocner:jocner@cluster0.axw6l.mongodb.net/swagger-typescript';



const conectarDB = async() => {
    try {
    
        await connect(DB_MONGO , {
           useNewUrlParser: true,
           useUnifiedTopology: true,
           useFindAndModify: false
        });
        console.log('DB Conectada');
    } catch(error) {
        console.log(error);
        process.exit(1);
    }
}


export default conectarDB;


