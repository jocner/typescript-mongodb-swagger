import { Router } from 'express';
import { newAvatar, allAvatar } from '../controllers/avatar.controller';
const router = Router();

router.post('/avatar',
   newAvatar
);

router.get('/avatar',
   allAvatar
);

export default router;