import { Schema, model, Document } from 'mongoose';


const avatarSchema = new Schema({
    name: {
        type: String,
        required: true,
        trim: true
    },
    createdAt: {
        type: Date,
        default: Date.now 
    },
    imageUrl: {
        type: String,
        required: false,
     },
    image: {
        type: String,
        required: false,
     },
     status: {
        type: String,
        require: false,
        enum: ['enabled', 'preview', 'disabled', 'deleted'],
        default: 'disabled',
      },
});


// export interface IUser extends Document {
//     name: string;
//     createdAt: Date;
//     imageUrl: string;
//     status: string;
// }



// export default model<IUser>('Avatar', avatarSchema);

export default model('Avatar', avatarSchema)